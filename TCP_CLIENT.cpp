﻿#include "stdafx.h"	
#include <WS2tcpip.h>
#include <iostream>
#include <string>

using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main() {
    
    // variable result untuk menyimpan nilai kembali saat memanggil fungsi
	int result; 
	
	WSAData wsData;
	
	string IP_SERVER = "127.0.0.1"; // set IP
	int DEFAULT_PORT = 7200; // set Port

	WSAStartup(MAKEWORD(2, 2), &wsData);

    // penyimpan informasi jaringan
	sockaddr_in hints;
	// set IPv4
	hints.sin_family = AF_INET;
	// set Port
	hints.sin_port = htons(DEFAULT_PORT);
	// memberikan informasi IP server
	inet_pton(AF_INET, IP_SERVER.c_str(), &hints.sin_addr); 
    // membuat socket
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0); 
    // meng-koneksi dengan server
	connect(sock, (SOCKADDR *)&hints, sizeof(hints)); 
	
	cout << "Connected to server..." << endl << endl;
	string text;
	cout << "Type -1 to disconnect" << endl;

    // pengiriman data
	do {
		cout << "Masukkan text: ";
		cin >> text;
        // proses mengirim data ke server
		send(sock, text.c_str(), 512, 0); 
	} while (text != "-1"); // mengirim data ke server selama teks tidak sama dengan -1

	closesocket(sock); // menutup socket client
	WSACleanup();

	return 0;
}
